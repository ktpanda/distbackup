[Version 1.0.20 (2023-02-25)](https://pypi.org/project/distbackup/1.0.20/)
=============================

* Fix --size argument with `disk add` ([c824512](https://gitlab.com/ktpanda/distbackup/-/commit/c8245126a8a4ba587bd65de2ce0a09ea265f3995))
* Fix `dsb restore --existing` ([24ec7f0](https://gitlab.com/ktpanda/distbackup/-/commit/24ec7f08a7468a333b7bfe68d0caab44a033d53a))
* Lock virtual paths for *any* I/O error that occurs when scanning directories ([43e53b2](https://gitlab.com/ktpanda/distbackup/-/commit/43e53b2f45e8148b3cbf338f91be1683ed12643b))


[Version 1.0.19 (2023-02-07)](https://pypi.org/project/distbackup/1.0.19/)
=============================

* Lock virtual path when `follow_symlinks` is enabled and broken symlink is encountered ([d178345](https://gitlab.com/ktpanda/distbackup/-/commit/d1783453b74bd899fb73a41e519791b0d84df1af))


[Version 1.0.18 (2023-02-07)](https://pypi.org/project/distbackup/1.0.18/)
=============================

* Add `--include`, `--exclude`, and `--simulate-drop` options to `ncdu` and `tree` commands ([32cf2d7](https://gitlab.com/ktpanda/distbackup/-/commit/32cf2d7044b2ec65005c50b86aff6225c645ecda))


[Version 1.0.17 (2023-01-06)](https://pypi.org/project/distbackup/1.0.17/)
=============================

* Fix typo in `get_tree` with `maxcopies` ([9813885](https://gitlab.com/ktpanda/distbackup/-/commit/9813885f395faffeaeb0388d01faa51625c5c5f2))


[Version 1.0.16 (2022-11-13)](https://pypi.org/project/distbackup/1.0.16/)
=============================

* Use ktpanda.cli module for command line parsing ([39734ea](https://gitlab.com/ktpanda/distbackup/-/commit/39734eaa0ed112c547b0924ca62d14024a4f6ab2))


[Version 1.0.15 (2022-10-09)](https://pypi.org/project/distbackup/1.0.15/)
=============================

* Add --copies, --mincopies, and --maxcopies to `tree` and `ncdu` commands ([fef9aae](https://gitlab.com/ktpanda/distbackup/-/commit/fef9aaea544c8b393cf0e26ab2eaa4f1d472dc02))


[Version 1.0.14 (2022-10-08)](https://pypi.org/project/distbackup/1.0.14/)
=============================

* verify: Move failed objects to a different directory for analysis and mark them deleted in database ([0110065](https://gitlab.com/ktpanda/distbackup/-/commit/0110065e325ddccfca5939c05bb1255a3afe24e4))


[Version 1.0.13 (2022-10-07)](https://pypi.org/project/distbackup/1.0.13/)
=============================

* Add --errors and --start options to verify command ([803c3fa](https://gitlab.com/ktpanda/distbackup/-/commit/803c3fae6991168f3ffb2080619f70c87d1c4db6))


[Version 1.0.12 (2022-10-06)](https://pypi.org/project/distbackup/1.0.12/)
=============================

* Fix `verify` command ([49d4e7b](https://gitlab.com/ktpanda/distbackup/-/commit/49d4e7b0d4bccd8398570ac45f5a731f9296a95b))


[Version 1.0.11 (2022-10-06)](https://pypi.org/project/distbackup/1.0.11/)
=============================

* Add NOT NULL constraints to last_path and last_modtime ([365ad92](https://gitlab.com/ktpanda/distbackup/-/commit/365ad9213b5997747255084e9e90f2321179f9e4))
* Always update nexus disk text after backup operation ([71fc15a](https://gitlab.com/ktpanda/distbackup/-/commit/71fc15acd511de06e0088cb0d1405ef5bdf102f8))


[Version 1.0.10 (2022-10-02)](https://pypi.org/project/distbackup/1.0.10/)
=============================

* Add --maxcopies option to `backup` command ([cecdd06](https://gitlab.com/ktpanda/distbackup/-/commit/cecdd06ef8de4043fbfc8ef36b5a825e0caf6745))
* Add --no-saturated option to `info` command ([cd78184](https://gitlab.com/ktpanda/distbackup/-/commit/cd781843af5c2e6cddce79210e874d92598c99f7))
* Add 'saturated size' to `info`, which shows the total size of all objects that are at the maximum number of copies ([5dc3e1c](https://gitlab.com/ktpanda/distbackup/-/commit/5dc3e1c0e393c279693b70ed3e2ec1c3becc9ece))
* Fix transposition that was inserting text into the 'refs' column ([1f89872](https://gitlab.com/ktpanda/distbackup/-/commit/1f8987251378971f066ad77f3f25331e912772ca))
* Add STRICT option to all tables ([b221fc4](https://gitlab.com/ktpanda/distbackup/-/commit/b221fc4bebd643ddcdb8370968d3910ce5034519))


[Version 1.0.8 (2022-09-25)](https://pypi.org/project/distbackup/1.0.8/)
============================

* Always log error when exception occurs ([6c5309d](https://gitlab.com/ktpanda/distbackup/-/commit/6c5309da2a57aa118034cc527d935531cbfd045b))


[Version 1.0.7 (2022-08-28)](https://pypi.org/project/distbackup/1.0.7/)
============================

* Add more documentation to README.md ([74f5a8c](https://gitlab.com/ktpanda/distbackup/-/commit/74f5a8c5c4418159e12cc9f2b2c2e775a300dfbf))
* Add more info when running 'disk add' ([90c22f6](https://gitlab.com/ktpanda/distbackup/-/commit/90c22f69faf80b8232ee1fbce993cb2f83160c20))


[Version 1.0.6 (2022-08-28)](https://pypi.org/project/distbackup/1.0.6/)
============================

* Add URL and description content type to setup.cfg ([d10fc44](https://gitlab.com/ktpanda/distbackup/-/commit/d10fc445a89f3d3b6ef0f65484e6366e54a85419))
* Remove bump_version.py (now using versionator) ([5d715c4](https://gitlab.com/ktpanda/distbackup/-/commit/5d715c4e751ee1d0ccf5b1b5c4b8de95eafac1db))


[Version 1.0.5 (2022-08-27)](https://pypi.org/project/distbackup/1.0.5/)
============================

* bump_version.py: Exit with error when uncommitted changes are present ([31a8096](https://gitlab.com/ktpanda/distbackup/-/commit//31a809617ed6c3cbd36e3eebea37276e8573661c))
* Remove redundant functions and move schema to separate module ([e5addee](https://gitlab.com/ktpanda/distbackup/-/commit//e5addeea8461eb488cb25e1ee7693b5b40baf3f2))


[Version 1.0.4 (2022-08-27)](https://pypi.org/project/distbackup/1.0.4/)
============================

* Fix PYPI links in CHANGELOG.md ([6925916](https://gitlab.com/ktpanda/distbackup/-/commit//6925916e9a147590b6c91e802dd02d574ecde5b8))


[Version 1.0.3 (2022-08-27)](https://pypi.org/project/distbackup/1.0.3/)
============================

* Add CHANGELOG.md for previous versions ([a0dedb4](https://gitlab.com/ktpanda/distbackup/-/commit//a0dedb4829ea69c9fd18b57593aa26c41767f608))
* Add changelog generation to bump_version.py ([286a34a](https://gitlab.com/ktpanda/distbackup/-/commit//286a34aec1e3934db21d36ea0e94564d91dd0b47))
* Remove hash_helper; it was moved to its own package at https://gitlab.com/ktpanda/hashcopy ([5f2ccab](https://gitlab.com/ktpanda/distbackup/-/commit//5f2ccab3b13793ed6d17569e4127642f19a0b236))


[Version 1.0.2 (2022-08-26)](https://pypi.org/project/distbackup/1.0.2/)
============================

* Add some documentation to README.md, still not complete ([7aa030b](https://gitlab.com/ktpanda/distbackup/-/commit//7aa030b1e88bf73ed8f1aa213f16fa64a8202cc4))
* Make hashcopy a requirement, but only for Linux ([ae67147](https://gitlab.com/ktpanda/distbackup/-/commit//ae671477cb8dc8bd658ddc93a5c12776325aea8d))
* Shook out some bugs from the refactor ([57a93d4](https://gitlab.com/ktpanda/distbackup/-/commit//57a93d475f44b7cf0374b071716be4bf2506f32f))


[Version 1.0.1 (2022-08-26)](https://pypi.org/project/distbackup/1.0.1/)
============================

* Add requirement for ktpanda_modules ([ac0c332](https://gitlab.com/ktpanda/distbackup/-/commit//ac0c332a9b0f6f3a91eb05c09d4658f81166f7be))


[Version 1.0.0 (2022-08-26)](https://pypi.org/project/distbackup/1.0.0/)
============================

* First release
* Refactor into a package with multiple modules ([abf037d](https://gitlab.com/ktpanda/distbackup/-/commit//abf037d42f9e4ac0b3c60f2887376c57c711f87d))
* Update logmgr ([69f7db6](https://gitlab.com/ktpanda/distbackup/-/commit//69f7db6bcbe5af5f652f91951cdb47124bef978b))
* Major refactor ([f1dcdaa](https://gitlab.com/ktpanda/distbackup/-/commit//f1dcdaa16f29c20ebe136d071270a149d9248aec))
* Add `debug info` command ([6d4f90a](https://gitlab.com/ktpanda/distbackup/-/commit//6d4f90a83b8239d1f64071575be04d1da740bd90))
* Replace deferred hash commit with generic version ([4c5b0a0](https://gitlab.com/ktpanda/distbackup/-/commit//4c5b0a031348143528c5f9ae378ea800c4208781))
* Add debug commands, rename `report` to `info` ([cc56b1b](https://gitlab.com/ktpanda/distbackup/-/commit//cc56b1b4ae68ea84487254dd90dc78ea584182a6))
* Add default log path and add --nolog option ([6756de1](https://gitlab.com/ktpanda/distbackup/-/commit//6756de1ee7dd6441f49da34987e198cb9e9744d4))
* Add `disk del` command ([04ffdea](https://gitlab.com/ktpanda/distbackup/-/commit//04ffdea92bb8f73839e1e76f510161c6b25c8e77))
* Remove commit_partial and just do a tranasaction after every file copy ([39db80f](https://gitlab.com/ktpanda/distbackup/-/commit//39db80fb276e7df8acb8a7ef3bf18a9ef25412a9))
* Add comments and code cleanup ([110dcd3](https://gitlab.com/ktpanda/distbackup/-/commit//110dcd3e58befbd1eefb624b2e135a0316f1d22f))
* Lots of other changes
